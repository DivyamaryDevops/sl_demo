import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Fib
{
        public static void main(String args[])throws IOException
        {

                int f1,f2,f3,i;
                f1=0;
                f2=1;
                System.out.println("Enter the value n:");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int n=Integer.parseInt(br.readLine());
                System.out.println("The fib series is:"+f1+","+f2);
                for(i=2;i<n;i++)
                {
                        f3=f1+f2;
                        f1= f2;
                        f2=f3;
                        System.out.println(f3+",");
                }
        }
}
